#!/bin/bash

read -p "Digite o nome do primeiro diretorio: " dir1
read -p "Digite o nome do segundo diretorio: " dir2
read -p "Digite o nome do terceiro diretorio: " dir3

xls=0
bmp=0
docx=0

tmp=$(ls $dir1 -1 | grep .xls | wc -l)
xls=$(($xls+$tmp))
tmp=$(ls $dir2 -1 | grep .xls | wc -l)
xls=$(($xls+$tmp))
tmp=$(ls $dir3 -1 | grep .xls | wc -l)
xls=$(($xls+$tmp))

tmp=$(ls $dir1 -1 | grep .bmp | wc -l)
bmp=$(($bmp+$tmp))
tmp=$(ls $dir2 -1 | grep .bmp | wc -l)
bmp=$(($bmp+$tmp))
tmp=$(ls $dir3 -1 | grep .bmp | wc -l)
bmp=$(($bmp+$tmp))

tmp=$(ls $dir1 -1 | grep .docx | wc -l)
docx=$(($docx+$tmp))
tmp=$(ls $dir2 -1 | grep .docx | wc -l)
docx=$(($docx+$tmp))
tmp=$(ls $dir3 -1 | grep .docx | wc -l)
docx=$(($docx+$tmp))

echo Arquivos xls: $xls
echo Arquivos bmp: $bmp
echo Arquivos docx: $docx

