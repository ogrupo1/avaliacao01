#!/bin/bash

mkdir $1 $2 $3 $4

touch README-md ./$1 ; date >./$1/README-md
touch README-md ./$2 ; date >./$2/README-md
touch README-md ./$3 ; date >./$3/README-md
touch README-md ./$4 ; date >./$4/README-md

#evitando arquivos duplicados
rm README-md
