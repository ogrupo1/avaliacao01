#!/bin/bash

v1=$(wc -l ./$1 | cut -c1-1)
v2=$(wc -l ./$2 | cut -c1-1)
v3=$(wc -l ./$3 | cut -c1-1)

echo $(($v1+$v2+$v3))
