#!/bin/bash

echo Existem basicamente 3 formas de atribuir variáveis:
sleep 3
echo
echo Uma delas é por meio da abribuição direta:
echo " 	variavel=valor"
sleep 3
echo
echo Outra delas é por meio do uso de comandos:
echo "	variavel=\$\(comando\)"
sleep 3
echo
echo Por fim é possível obtê-las por meio de parametros do script:
echo "	variavel=\$1 [para o primeiro parametro, por exemplo]"
sleep 3
echo
echo Embora não se trate de uma atribuição direta, também existem as variaveis de sistema:
echo "	\$USER -mostra o usuario"
sleep 3
echo
echo Esse é um resumo das variáveis no shell script
